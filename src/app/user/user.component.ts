import { HttpClient } from '@angular/common/http';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { User } from '../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnChanges {
  @Input()
  id: number;
  user$: Observable<User>;

  constructor(private http: HttpClient, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    if (this.id) {
      this.user$ = this.getUser(this.id);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const current = changes.id.currentValue;
    if (current !== changes.id.previousValue) {
      this.user$ = this.getUser(current);
    }
  }

  getUser(id) {
    return this.http.get<User>(`/api/users/${id}`);
  }
}
