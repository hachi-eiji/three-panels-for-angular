import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { User } from '../user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss', './users.component.sp.scss']
})
export class UsersComponent implements OnInit {
  users: Observable<User>;
  id: number;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.users = this.http.get<User>('/api/users');
    this.id = this.route.snapshot.params.id;
    this.router.events.subscribe(e => {
      if (e instanceof ActivationEnd) {
        this.id = e.snapshot.params.id;
      }
    });
  }

  showDetail(e: Event, user: User) {
    e.preventDefault();
    this.id = user.id;
  }
}
