import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable()
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const users = this.createUsers();
    return {
      users
    };
  }

  private createUsers() {
    const u = [];
    for (let i = 1; i <= 50; i++) {
      u.push({
        id: i,
        name: `user ${i}`
      });
    }
    return u;
  }
}
