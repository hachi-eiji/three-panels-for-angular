import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.scss', './menubar.component.sp.scss']
})
export class MenubarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
